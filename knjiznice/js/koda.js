
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";
var teza = [];
var visina = [];
var spO2 = [];
var datumrojstva1;
var podatki;
var oznake;
/* global L, distance, closestLayer */

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 /*global $*/
function generirajPodatke(stPacienta) {
  ehrId = "";
  var ime;
  var priimek;
  var datumRojstva;
  
  if (stPacienta == 1) {
    ime = "Marko";
    priimek = "Kranjc";
    datumRojstva = "1992-02-15"+"T00:00:00.000Z";
  } else if (stPacienta == 2) {
    ime = "Anton";
    priimek = "Novak";
    datumRojstva = "1978-06-09"+"T00:00:00.000Z";
  } else{
    ime = "Mateja";
    priimek = "Vidmar";
    datumRojstva = "1985-03-03"+"T00:00:00.000Z";
  }  
  
  $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        var option = document.createElement("option");
        option.text =ehrId ;
        option.value=ime+" "+priimek;
        document.getElementById("izbiraPacienta").appendChild(option);
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#sporocilo1").html("<span class='obvestilo " +
                "label label-success fade-in'>Uspešno kreiran EHR '" +
                ehrId + "'.</span>");
              if(stPacienta==1){
                  dodajMeritveVitalnihZnakov(ehrId, "2014-03-17T10:32", "182.3", "105.2","98" );
                  dodajMeritveVitalnihZnakov(ehrId, "2015-04-28T11:07", "183.0", "106.1","96" );
                  dodajMeritveVitalnihZnakov(ehrId, "2016-02-20T09:38", "183.1", "101.7","97" );
                  dodajMeritveVitalnihZnakov(ehrId, "2017-05-20T11:33", "182.8", "107.4","96" );
                }

              else if(stPacienta==2){
                  dodajMeritveVitalnihZnakov(ehrId, "2014-02-25T08:30", "178.3", "83.5", "97" );
                  dodajMeritveVitalnihZnakov(ehrId, "2015-06-07T09:05", "179.1", "82.1", "98" );
                  dodajMeritveVitalnihZnakov(ehrId, "2016-08-13T09:27", "179.0", "84.5", "97" );
                  dodajMeritveVitalnihZnakov(ehrId, "2017-01-17T08:36", "179.0", "83.6", "95" );
                }

              else if(stPacienta==3){
                  dodajMeritveVitalnihZnakov(ehrId, "2014-09-13T07:32", "171.2", "64.2", "97" );
                  dodajMeritveVitalnihZnakov(ehrId, "2015-11-07T07:59", "170.6", "63.6", "99" );
                  dodajMeritveVitalnihZnakov(ehrId, "2016-06-21T08:15", "170.7", "64.3", "96" );
                  dodajMeritveVitalnihZnakov(ehrId, "2017-05-28T09:20", "171.1", "63.2", "98" );
              }              
            }
          },
          error: function(err) {
          	$("#sporocilo1").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
  return ehrId;
}

function dodajMeritveVitalnihZnakov(ehrId, datumInUra, telesnaVisina, telesnaTeza, nasicenostKrviSKisikom) {
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        console.log(res.meta.href);
      },
      error: function(err) {
        console.log(err.meta.href);
      }
		});
}

function spremeniVrednostPolja(){
  visina=[];
  teza=[];
  spO2=[];
  var t =$("#izbiraPacienta option:selected").val(); 
  $("#prikaziIzbranega").val(t);
} 
 
function preberiEHRodBolnika(stevilka){
  document.getElementById("izbiraMeritve").innerHTML="";
  var option = document.createElement("option");
  option.value="";
  document.getElementById("izbiraMeritve").appendChild(option);
  if(stevilka==1){
    var ehrId = $("#izbiraPacienta option:selected").text();
  }
  else{
    var ehrId = $("#preberiEHRid").val();
  }
  $.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {
          "Authorization": getAuthorization()
        },
	    	success: function (data) {
  				var party = data.party;
  				console.log(party);
  				$("#imeIzbranega").val(party.firstNames);
  				$("#priimekIzbranega").val(party.lastNames);
  				$("#rojstvoIzbranega").val(party.dateOfBirth);
  				$("#ehrIdIzbranega").val(party.additionalInfo.ehrId);
  				
  				datumrojstva1=party.dateOfBirth;
  				
  				$.ajax({
    				  url: baseUrl + "/view/" + ehrId + "/" + "weight",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    			    	  teza.push(res);
    			    	  for(var i = 0;i<teza[0].length;i++){
		                var option = document.createElement("option");
                    option.text =teza[0][i].time;
                    option.value=i;
                    document.getElementById("izbiraMeritve").appendChild(option);
		                }
    			    	} else {
    			    		$("#sporocilo3").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
    			    	}
    			    },
    			    error: function() {
    			    	$("#sporocilo3").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  					
  					
  					
  					
  					$.ajax({
    			    url: baseUrl + "/view/" + ehrId + "/" + "height",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    			    	  visina.push(res);
    			    	} else {
    			    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
    			    	}
    			    },
    			    error: function() {
    			    	$("#sporocilo3").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  					
  					
  					$.ajax({
    			    url: baseUrl + "/view/" + ehrId + "/" + "spO2",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    			    	  spO2.push(res);
    			    	} else {
    			    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
    			    	}
    			    },
    			    error: function() {
    			    	$("#sporocilo3").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
	    	},
	    	error: function(err) {
	    		$("#sporocilo3").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
  }
function prikaziMeritev(){
  var t =$("#izbiraMeritve option:selected").val();
  var d=$("#izbiraMeritve option:selected").text();
  $("#weight").val(teza[0][t].weight+" "+teza[0][t].unit);
  $("#izbranaTeza").text(teza[0][t].weight+" "+teza[0][t].unit);
  $("#height").val(visina[0][t].height+" "+visina[0][t].unit);
  $("#spO2").val(spO2[0][t].spO2);
  
  var split1 = d.split("T");
  var split2 = split1[0];
  var split3 = split2.split("-");
  var dd = split3[2];
  var mm = split3[1];
  var yyyy = split3[0];
  var starost;
  var dSplit=datumrojstva1.split("-");
  if(mm>dSplit[1]){
    starost=yyyy-dSplit[0];
  }
  else if(mm==dSplit[1]){
    if(dd>=dSplit[2]){
      starost=yyyy-dSplit[0];
    }
    else{
      starost=yyyy-dSplit[0]-1;
    }
  }
  else{
    starost=yyyy-dSplit[0]-1;
  }
  $("#starost").val(starost);
}



// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

window.addEventListener("load", function (){
  
});

function bolnisnice (){
  var mapa;
  var layers = [];
  var a;
  var greenIcon = new L.Icon({
    iconUrl: 'knjiznice/css/images/marker-icon-green.png',
    shadowUrl: 'knjiznice/css/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });
  var blueIcon = new L.Icon({
    iconUrl: 'knjiznice/css/images/marker-icon.png',
    shadowUrl: 'knjiznice/css/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });


  var mapOptions = {
    center: [46.05004, 14.46931],
    zoom: 12
    // maxZoom: 3
  };
  
  mapa = new L.map('mapa_id', mapOptions);
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
  mapa.addLayer(layer);
  mapa.invalidateSize;
  
  podatki(function (data) {
    a = L.geoJSON(data,{
      color: 'blue',
      onEachFeature: onEachFeature
    }).addTo(mapa);
    function onEachFeature(feature,layer){
      layers.push(layer);
      layer.bindPopup("<div>" + feature.properties.name + "</div>\
                       <div>" + feature.properties['addr:street'] + " "+feature.properties['addr:housenumber']+ ", "+feature.properties['addr:city']+ "</div>");
    }
  });
  
  function obKlikuNaMapo(e){
    var latlng = e.latlng;
    var najblizji = L.GeometryUtil.closestLayer(mapa,layers,latlng);
    
    a.eachLayer(function(layer){
      if(layer.feature.geometry.type=="Point"){
        layer.setIcon(blueIcon);
      }
      else{
        a.resetStyle(layer);
      }
    });
    
    
    if(najblizji.layer.feature.geometry.type == "Point"){
      najblizji.layer.setIcon(greenIcon);
    }
    else {
      najblizji.layer.setStyle({
        fillColor: 'green',
        color: 'green'             
      });
    }
  }
  mapa.on('click', obKlikuNaMapo);
}

function podatki (callback){
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open('GET', 'https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json', true);
  xobj.onreadystatechange = function () {
    if (xobj.readyState == 4 && xobj.status == "200") {
      var json = JSON.parse(xobj.responseText);
      callback(json);
     }
    };
    xobj.send(null);
}
/*global fetch*/
function index(){

  document.getElementById("isci").addEventListener("click", function (response){
    var ul = document.getElementById("rezultati");
    ul.innerHTML="";
    var text=document.getElementById("vnosno_polje").value;
    fetch("https://api.edamam.com/api/food-database/parser?app_id=e5ad1545&app_key=28287c02d5f1032e8e3c4d4e3dae07eb&ingr=".concat(text)).then(function(response) {
      return response.json();
    }).then(function(response){
      response.hints.forEach(function(d){
        var rez = document.createElement("li");
        rez.className = "list-group-item";
        rez.innerHTML = d.food.label+"<br>="+d.food.nutrients.ENERC_KCAL.toFixed(2)+" kcal";
        rez.setAttribute("value", d.food.nutrients.CHOCDF+"/"+d.food.nutrients.FAT+"/"+d.food.nutrients.PROCNT);
        ul.appendChild(rez);
      });
    });
  });
  var ctx = document.getElementById('myChart');
  var options={};
  var myPieChart = new Chart(ctx, {
    type: 'pie',
    data: {
        datasets:[{
          data: [],
          backgroundColor: ["#e6194B", "#3cb44b", "#ffe119", "#4363d8", "#f58231", "#911eb4", "#42d4f4", "#f032e6", "#bfef45", "#fabebe", "#469990", 
  "#e6beff", "#9A6324", "#fffac8", "#800000", "#aaffc3", "#808000", "#ffd8b1", "#000075", "#a9a9a9","#000000"]
        }],
        labels:[]
    },
    options: options
   });
  $('#rezultati').on('click', 'li', function() {
      var skupnoK = 0;
      var oh = 0;
      var mascobe = 0;
      var beljakovine = 0;
      $(this).appendTo('#izbrani');
      podatki = [];
      oznake = [];
      $('#izbrani li').each(function(){
        var podatek = $(this).attr("value");
        var podatek1 = podatek.split("/");
        var split = $(this).text().split("=");
        var split2 = split[1].split(" ");
        var lab = split[0];
        var val = split2[0];
        oh+=parseInt(podatek1[0]);
        mascobe+=parseInt(podatek1[1]);
        beljakovine+=parseInt(podatek1[2]);
        skupnoK+=parseInt(val);
        podatki.push(val);
        oznake.push(lab);
      });
      addData(myPieChart, oznake, podatki);
      document.getElementById("skupnoKalorij").innerHTML=skupnoK;
      document.getElementById("skupnoKalorijZaVadbe").innerHTML=skupnoK;
      document.getElementById("oh").value = oh+" g";
      document.getElementById("mascobe").value = mascobe+" g";
      document.getElementById("beljakovine").value = beljakovine+" g";
    });
    $('#izbrani').on('click', 'li', function() {
      var skupnoK = 0;
      var oh = 0;
      var mascobe = 0;
      var beljakovine = 0;
      $(this).appendTo('#rezultati');
      podatki=[];
      oznake = [];
      $('#izbrani li').each(function(){
        var podatek = $(this).attr("value");
        var podatek1 = podatek.split("/");
        var split = $(this).text().split("=");
        var split2 = split[1].split(" ");
        var lab = split[0];
        var val = split2[0];
        oh+=parseInt(podatek1[0]);
        mascobe+=parseInt(podatek1[1]);
        beljakovine+=parseInt(podatek1[2]);
        skupnoK+=parseInt(val);
        podatki.push(val);
        oznake.push(lab);
      });
      addData(myPieChart, oznake, podatki);
      document.getElementById("skupnoKalorij").innerHTML=skupnoK;
      document.getElementById("skupnoKalorijZaVadbe").innerHTML=skupnoK;
      document.getElementById("oh").value = oh+" g";
      document.getElementById("mascobe").value = mascobe+" g";
      document.getElementById("beljakovine").value = beljakovine+" g";
    });
    
  var slider = new Slider('#tezavnost', {
	  formatter: function(value) {
	    if(value>6){
	      $("#tezavnost1").attr("class", "label label-pill label-danger").text("Težka");
	      if(value<8.8){
	        $("#vadba").text("Tek (6.5 km/h)");
	      }
	      else if(8.8<=value && value<10.0){
	        $("#vadba").text("Tek (9 km/h)");
	      }
	      else if(10<=value && value<10.5){
	        $("#vadba").text("Skakanje s kolebnico (84 skokov/min)");
	      }
	      else{
	        $("#vadba").text("Tek (11 km/h)");
	      }
	    }
	    else if(value<3){
	      $("#tezavnost1").attr("class", "label label-pill label-success").text("Lahka");
	     if(value>0.9){
	        $("#vadba").text("Sprehod (2.7 km/h)");
	     }
	     else{
	       $("#vadba").text("Spanje");
	     }
	    }
	    else{
	      $("#tezavnost1").attr("class", "label label-pill label-warning").text("Srednja");
	      if(value<=3.6){
	        $("#vadba").text("Sprehod (5.5 km/h)");
	      }
	      else{
	        $("#vadba").text("Kolesarjenje (do 20 km/h)");
	      }
	    }
		  return 'Izbrana težavnost: ' + value;
	  }
  });
}

function addData(chart, l, d,c) {
    chart.data.datasets[0].data=d;
    chart.data.labels = l;
    chart.update();
}
function izracunajCas(kalorije, tezavnost){
   var kalorije = $("#skupnoKalorijZaVadbe").text();
   var teza = $("#izbranaTeza").text();
   var tezavnost = $("#tezavnost").val();
   if(teza=="" || kalorije==""){
     $("#sporocilo4").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Manjkajoči podatki!</span>");
   }
   else{
     var tezaInt = parseInt(teza);
     var kalorijeInt=parseInt(kalorije);
     var kalNaUro = tezaInt*tezavnost;
     var cas = kalorijeInt/kalNaUro;
     var cas1 = cas.toFixed(2);
     $("#cas").text(cas1);
   }
 }
